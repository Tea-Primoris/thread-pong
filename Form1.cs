﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Media;

namespace Thread_Pong
{
    public partial class Form1 : Form
    {
        private readonly ThreadPong threadPong;
        private readonly System.Timers.Timer timer;
        private readonly System.Timers.Timer startTimer;
        private bool countdownVisible;
        private bool goalVisible;
        private bool gameStarted;
        public int counter;

        public Form1()
        {
            InitializeComponent();
            threadPong = new ThreadPong();

            timer = new System.Timers.Timer(1)
            {
                Enabled = true
            };
            timer.Elapsed += OnTimerEvent;

            counter = 3;
            startTimer = new System.Timers.Timer(1000)
            {
                Enabled = true
            };
            startTimer.Elapsed += StartGame;

            countdownVisible = true;
            goalVisible = false;

            System.Reflection.PropertyInfo aProp = typeof(System.Windows.Forms.Control)
                .GetProperty("DoubleBuffered", System.Reflection.BindingFlags.NonPublic |
                System.Reflection.BindingFlags.Instance);
            aProp.SetValue(this, true, null);
        }

        public void OnTimerEvent(Object source, System.Timers.ElapsedEventArgs e)
        {
            if (gameStarted)
            {
                threadPong.ball.UpdatePosition();

                if (!threadPong.ball.moving && !startTimer.Enabled)
                {
                    countdownVisible = true;
                    startTimer.Enabled = true;
                    goalVisible = true;
                }

                this.Invalidate();
            }
        }

        public void StartGame(Object source, System.Timers.ElapsedEventArgs e)
        {
            if (gameStarted)
            {
                counter--;
                if (counter < 0)
                {
                    countdownVisible = false;
                    goalVisible = false;
                    threadPong.ball.moving = true;
                    startTimer.Enabled = false;
                    counter = 3;
                }
            }
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            threadPong.Paint(e);

            label1.Text = "Score: " + threadPong.p1score.ToString();
            label2.Text = "Score: " + threadPong.p2socre.ToString();
            label3.Text = counter.ToString();
            label3.Visible = countdownVisible;
            label4.Visible = goalVisible;
            label5.Visible = !gameStarted;
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            threadPong.KeyDown(e);
        }

        private void Form1_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Space) gameStarted = true;
            threadPong.KeyUp(e);
        }
    }

    class ThreadPong
    {
        public Player p1;
        public Player p2;
        public Ball ball;

        public int p1score;
        public int p2socre;

        public ThreadPong()
        {
            p1 = new Player(10, Keys.S, Keys.W);
            p2 = new Player(800, Keys.Down, Keys.Up);
            ball = new Ball(p1, p2, this);

            p1score = 0;
            p2socre = 0;
        }

        public void Paint(PaintEventArgs e)
        {
            e.Graphics.FillRectangle(Brushes.Black, p1.racket);
            e.Graphics.FillRectangle(Brushes.Black, p2.racket);
            e.Graphics.FillRectangle(Brushes.Red, ball.ball);
            e.Graphics.FillRectangle(Brushes.Gray, ball.topWall);
            e.Graphics.FillRectangle(Brushes.Gray, ball.botWall);
        }

        public void KeyDown(KeyEventArgs e)
        {
            p1.KeyDown(e);
            p2.KeyDown(e);
        }

        public void KeyUp(KeyEventArgs e)
        {
            p1.KeyUp(e);
            p2.KeyUp(e);
        }

        public void Score(int player)
        {
            if (player == 1)
            {
                p1score++;
            }
            else if (player == 2)
            {
                p2socre++;
            }

            ball.moving = false;
        }
    }

    class Player
    {
        public Rectangle racket;
        private readonly Size racketSize;
        private readonly Point racketPoint;
        private readonly Keys keyUp;
        private readonly Keys keyDown;

        public bool movingUp;
        public bool movingDown;
        private readonly int speed;

        private readonly System.Threading.Timer updateTimer;

        public Player(int x, Keys downKey, Keys upKey)
        {
            racketSize = new Size(10, 100);
            racketPoint = new Point(x, 170);
            racket = new Rectangle(racketPoint, racketSize);

            keyUp = upKey;
            keyDown = downKey;

            speed = 3;
            movingUp = false;
            movingDown = false;

            updateTimer = new System.Threading.Timer(new TimerCallback(UpdatePos), null, 0, 1);
        }

        private void UpdatePos(object state)
        {
            if (movingDown && movingUp)
            {
                return;
            }
            else if (movingUp && !(racket.Top<40))
            {
                racket.Y -= speed;
                if (racket.Top < 40) racket.Y = 40;
            }
            else if (movingDown && !(racket.Bottom>470))
            {
                racket.Y += speed;
                if (racket.Bottom > 470) racket.Y = 470 - racket.Height;
            }
        }

        public void KeyDown(KeyEventArgs e)
        {
            if( e.KeyCode == keyUp)
            {
                movingUp = true;
            }
            else if (e.KeyCode == keyDown)
            {
                movingDown = true;
            }
        }

        public void KeyUp(KeyEventArgs e)
        {
            if (e.KeyCode == keyUp)
            {
                movingUp = false;
            }
            else if (e.KeyCode == keyDown)
            {
                movingDown = false;
            }
        }
    }

    class Ball
    {
        public Rectangle ball;
        private Size ballSize;
        private Point ballPoint;

        public Rectangle topWall;
        private Point topWallPoint;
        public Rectangle botWall;
        private Point botWallPoint;
        private Size wallSize;

        private int yspeed;
        private int xspeed;
        private bool dy;
        private bool dx;
        public bool moving;

        private readonly Player player1;
        private readonly Player player2;
        private readonly ThreadPong threadPong;

        private readonly System.Timers.Timer speedTimer;

        private readonly SoundPlayer sound1;
        private readonly SoundPlayer sound2;
        private readonly SoundPlayer wallsound;
        private readonly SoundPlayer scoreSound;

        public Ball(Player p1, Player p2, ThreadPong pong)
        {
            ballPoint = new Point(400, 225);
            ballSize = new Size(20, 20);
            ball = new Rectangle(ballPoint, ballSize);

            wallSize = new Size(820, 40);

            topWallPoint = new Point(0, 0);
            topWall = new Rectangle(topWallPoint, wallSize);

            botWallPoint = new Point(0, 470);
            botWall = new Rectangle(botWallPoint, wallSize);

            speedTimer = new System.Timers.Timer(10000);
            speedTimer.Elapsed += IncreaseSpeed;
            speedTimer.Enabled = true;

            dy = new bool();
            dx = new bool();

            player1 = p1;
            player2 = p2;
            threadPong = pong;

            sound1 = new SoundPlayer(Properties.Resources.racket_sound);
            sound2 = new SoundPlayer(Properties.Resources.racket_sound2);
            wallsound = new SoundPlayer(Properties.Resources.wall_sound);
            scoreSound = new SoundPlayer(Properties.Resources.score_sound);



            moving = false;

            Reset();
        }

        public void Reset()
        {            
            Random rng = new Random();
            yspeed = rng.Next(2, 5);
            xspeed = rng.Next(2, 5);
            dy = rng.Next(2).Equals(1);
            dx = rng.Next(2).Equals(1);
            ball.X = 400;
            ball.Y = 225;
        }

        private void IncreaseSpeed(Object source, System.Timers.ElapsedEventArgs e)
        {
            xspeed++;
            yspeed++;
        }

        public void UpdatePosition()
        {
            if (!moving) return;
            if (dy)
            {
                ball.Y -= yspeed;
                if (ball.Top < topWall.Bottom)
                {
                    wallsound.Play();
                    dy = false;
                }
            }
            else
            {
                ball.Y += yspeed;
                if (ball.Bottom > botWall.Top)
                {
                    wallsound.Play();
                    dy = true;
                }
            }

            if (dx)
            {
                ball.X += xspeed;
                if (player2.racket.IntersectsWith(ball))
                {
                    dx = false;
                    sound1.Play();
                }

                if (ball.Left > 820)
                {
                    scoreSound.Play();
                    threadPong.Score(1);
                    Reset();
                }
            }
            else
            {
                ball.X -= xspeed;
                if (player1.racket.IntersectsWith(ball))
                {
                    dx = true;
                    sound2.Play();
                }

                if (ball.Right < 0)
                {
                    scoreSound.Play();
                    threadPong.Score(2);
                    Reset();
                }
            }
        }
    }
}
